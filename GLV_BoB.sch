EESchema Schematic File Version 2
LIBS:power
LIBS:device
LIBS:transistors
LIBS:conn
LIBS:linear
LIBS:regul
LIBS:74xx
LIBS:cmos4000
LIBS:adc-dac
LIBS:memory
LIBS:xilinx
LIBS:microcontrollers
LIBS:dsp
LIBS:microchip
LIBS:analog_switches
LIBS:motorola
LIBS:texas
LIBS:intel
LIBS:audio
LIBS:interface
LIBS:digital-audio
LIBS:philips
LIBS:display
LIBS:cypress
LIBS:siliconi
LIBS:opto
LIBS:atmel
LIBS:contrib
LIBS:valves
LIBS:w_connectors
LIBS:w_relay
LIBS:GLV_BoB-cache
EELAYER 25 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "GLV_Power/VSCADA/Safety BoB"
Date "2016-04-01"
Rev "0.1"
Comp "Lafayette College"
Comment1 "Spring 2016"
Comment2 "ECE 492"
Comment3 "Supervisor: Chris Nadovich"
Comment4 "Engineer: Joe Cericola"
$EndDescr
$Comp
L DB9 J9
U 1 1 56EA4774
P 3600 6450
F 0 "J9" H 3600 7000 70  0000 C CNN
F 1 "DB9" H 3600 5900 70  0000 C CNN
F 2 "custom:DB9FC_Mod" H 3600 6450 60  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/assmann-wsw-components/A-DF-09-A-KG-T4S/AE10924-ND/1241802" H 3600 6450 60  0001 C CNN
	1    3600 6450
	0    1    1    0   
$EndComp
$Comp
L CONN_01X04 J3
U 1 1 56EA4A8E
P 1200 2650
F 0 "J3" H 1200 2900 50  0000 C CNN
F 1 "CONN_01X04" V 1300 2650 50  0000 C CNN
F 2 "custom:TerminalBlock_Pheonix_MPT-5.08mm_4pol" H 1200 2650 60  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/on-shore-technology-inc/OSTTC042162/ED2611-ND/614560" H 1200 2650 60  0001 C CNN
	1    1200 2650
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X04 J4
U 1 1 56EA4B05
P 1200 3350
F 0 "J4" H 1200 3600 50  0000 C CNN
F 1 "CONN_01X04" V 1300 3350 50  0000 C CNN
F 2 "custom:TerminalBlock_Pheonix_MPT-5.08mm_4pol" H 1200 3350 60  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/on-shore-technology-inc/OSTTC042162/ED2611-ND/614560" H 1200 3350 60  0001 C CNN
	1    1200 3350
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X06 J5
U 1 1 56EA4C68
P 1200 4150
F 0 "J5" H 1200 4500 50  0000 C CNN
F 1 "CONN_01X06" V 1300 4150 50  0000 C CNN
F 2 "custom:TerminalBlock_Pheonix_MPT-5.08mm_6pol" H 1200 4150 60  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/on-shore-technology-inc/OSTTC062162/ED2613-ND/614562" H 1200 4150 60  0001 C CNN
	1    1200 4150
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X04 J6
U 1 1 56EA4CE9
P 1200 4950
F 0 "J6" H 1200 5200 50  0000 C CNN
F 1 "CONN_01X04" V 1300 4950 50  0000 C CNN
F 2 "custom:TerminalBlock_Pheonix_MPT-5.08mm_4pol" H 1200 4950 60  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/on-shore-technology-inc/OSTTC042162/ED2611-ND/614560" H 1200 4950 60  0001 C CNN
	1    1200 4950
	-1   0    0    -1  
$EndComp
$Comp
L CONN_01X02 J7
U 1 1 56EA4DA0
P 1200 5550
F 0 "J7" H 1200 5700 50  0000 C CNN
F 1 "CONN_01X02" V 1300 5550 50  0000 C CNN
F 2 "custom:TerminalBlock_Pheonix_MPT-5.08mm_2pol" H 1200 5550 60  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/on-shore-technology-inc/OSTTC022162/ED2609-ND/614558" H 1200 5550 60  0001 C CNN
	1    1200 5550
	-1   0    0    -1  
$EndComp
$Comp
L D_Schottky D2
U 1 1 56EAE3D2
P 5100 4000
F 0 "D2" H 5100 4100 50  0000 C CNN
F 1 "D_Schottky" H 5100 3900 50  0000 C CNN
F 2 "Diodes_SMD:SMA_Handsoldering" H 5100 4000 60  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/micro-commercial-co/SK310A-LTP/SK310A-LTPMSCT-ND/2642066" H 5100 4000 60  0001 C CNN
	1    5100 4000
	0    -1   -1   0   
$EndComp
$Comp
L RELAY_HJR-3FF_H RLY2
U 1 1 56F00550
P 5850 3950
F 0 "RLY2" H 5850 4200 60  0000 C CNN
F 1 "RELAY_HJR-3FF_H" H 5850 3715 60  0000 C CNN
F 2 "custom:Relay_G5LE_SPST" H 5850 3950 60  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/omron-electronics-inc-emc-div/G5LE-1A4-DC24/Z2254-ND/369017" H 5850 3950 60  0001 C CNN
	1    5850 3950
	0    -1   -1   0   
$EndComp
Text Label 1800 1350 0    60   ~ 0
GND
Text Label 1800 5500 0    60   ~ 0
CAN_H
Text Label 1800 5600 0    60   ~ 0
CAN_L
Text Label 1800 1900 0    60   ~ 0
+24V
Text Label 1800 4100 0    60   ~ 0
+24V
Text Label 1800 4000 0    60   ~ 0
+12V
Text Label 1800 3900 0    60   ~ 0
+5V
Text Label 1800 4200 0    60   ~ 0
CAN_H
Text Label 1800 4300 0    60   ~ 0
CAN_L
Text Label 1800 4400 0    60   ~ 0
GND
Text Label 1800 4900 0    60   ~ 0
GND
Text Label 1800 5100 0    60   ~ 0
GND
Text Label 1800 4800 0    60   ~ 0
+24V
$Comp
L CONN_02X05 P1
U 1 1 56F060EA
P 9050 1500
F 0 "P1" H 9050 1800 50  0000 C CNN
F 1 "CONN_02X05" H 9050 1200 50  0000 C CNN
F 2 "custom:Pin_Header_Straight_2x05" H 9050 1106 60  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/amphenol-fci/67997-410HLF/609-3243-ND/1878475" H 9050 300 60  0001 C CNN
	1    9050 1500
	1    0    0    -1  
$EndComp
Text Label 8400 1700 0    60   ~ 0
GND
Text Label 9700 1300 0    60   ~ 0
+5V
Text Label 8400 1500 0    60   ~ 0
SCL
Text Label 8400 1400 0    60   ~ 0
SDA
Text Label 1800 2000 0    60   ~ 0
MReset_A
Text Label 1800 2100 0    60   ~ 0
MReset_B
NoConn ~ 8800 1300
NoConn ~ 9300 1600
NoConn ~ 9300 1700
Text Label 1800 3200 0    60   ~ 0
+24V
Text Label 1800 3300 0    60   ~ 0
SL
Text Label 1800 3500 0    60   ~ 0
GND
Text Label 9700 1400 0    60   ~ 0
+5V
Text Label 9700 1500 0    60   ~ 0
GND
Text Label 1800 2500 0    60   ~ 0
+5V
Text Label 1800 2600 0    60   ~ 0
SDA
Text Label 1800 2700 0    60   ~ 0
SCL
Text Label 1800 2800 0    60   ~ 0
GND
Text Label 5400 4550 0    60   ~ 0
MReset_B
Text Label 1800 3400 0    60   ~ 0
MReset_B
Text Label 1800 5000 0    60   ~ 0
MReset_B
Text Label 5400 3350 0    60   ~ 0
GND
Text Label 6000 3350 0    60   ~ 0
MReset_B
Text Label 3500 5800 0    60   ~ 0
CAN_H
Text Label 3400 5700 0    60   ~ 0
CAN_L
$Comp
L CONN_01X02 J8
U 1 1 56FB237E
P 1200 6050
F 0 "J8" H 1200 6200 50  0000 C CNN
F 1 "CONN_01X02" V 1300 6050 50  0000 C CNN
F 2 "custom:Molex_2POS_conn_hdr" H 1200 6050 60  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/molex-llc/0015912025/WM1340-ND/531447" H 1200 6050 60  0001 C CNN
	1    1200 6050
	-1   0    0    -1  
$EndComp
Text Label 1800 6000 0    60   ~ 0
+5V
Text Label 1800 6100 0    60   ~ 0
GND
$Comp
L D_Schottky D1
U 1 1 56FB2B3E
P 3400 4000
F 0 "D1" H 3400 4100 50  0000 C CNN
F 1 "D_Schottky" H 3400 3900 50  0000 C CNN
F 2 "Diodes_SMD:SMA_Handsoldering" H 3400 4000 60  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/micro-commercial-co/SK310A-LTP/SK310A-LTPMSCT-ND/2642066" H 3400 4000 60  0001 C CNN
	1    3400 4000
	0    -1   -1   0   
$EndComp
$Comp
L RELAY_HJR-3FF_H RLY1
U 1 1 56FB2B44
P 4150 3950
F 0 "RLY1" H 4150 4200 60  0000 C CNN
F 1 "RELAY_HJR-3FF_H" H 4150 3715 60  0000 C CNN
F 2 "custom:Relay_G5LE_SPST" H 4150 3950 60  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/omron-electronics-inc-emc-div/G5LE-1A-DC5/Z3115-ND/369007" H 4150 3950 60  0001 C CNN
	1    4150 3950
	0    -1   -1   0   
$EndComp
$Comp
L VYB20W-Q24-S5 U1
U 1 1 56FB4A84
P 3900 1500
F 0 "U1" H 3900 1900 60  0000 C CNN
F 1 "VYB20W-Q24-S5" H 3900 1100 60  0000 C CNN
F 2 "custom:DC-DC_24-5" H 3900 1500 60  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/cui-inc/PYB20-Q24-S5/102-2927-ND/4289506" H 3900 1500 60  0001 C CNN
	1    3900 1500
	1    0    0    -1  
$EndComp
$Comp
L VYC30W-Q24-S12 U2
U 1 1 56FB4BD6
P 6600 1500
F 0 "U2" H 6600 2000 60  0000 C CNN
F 1 "VYC30W-Q24-S12" H 6600 1000 60  0000 C CNN
F 2 "custom:DC-DC_24-12" H 6600 1500 60  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/cui-inc/PYB30-Q24-S12/102-2943-ND/4289546" H 6600 1500 60  0001 C CNN
	1    6600 1500
	1    0    0    -1  
$EndComp
Text Label 2800 1450 0    60   ~ 0
GND
Text Label 2800 1550 0    60   ~ 0
+24V
Text Label 5000 1300 0    60   ~ 0
GND
Text Label 5000 1700 0    60   ~ 0
+5V
Text Label 5500 1550 0    60   ~ 0
+24V
Text Label 5500 1450 0    60   ~ 0
GND
Text Label 7700 1400 0    60   ~ 0
GND
Text Label 7700 1600 0    60   ~ 0
+12V
NoConn ~ 3200 6000
NoConn ~ 3300 6000
NoConn ~ 3600 6000
NoConn ~ 3700 6000
NoConn ~ 3800 6000
NoConn ~ 3900 6000
NoConn ~ 4000 6000
NoConn ~ 7300 1200
NoConn ~ 5900 1250
NoConn ~ 4600 1500
NoConn ~ 3200 1300
Text Label 8800 3800 0    60   ~ 0
Vout
Text Label 3700 4550 0    60   ~ 0
Vout
Text Label 3700 3350 0    60   ~ 0
GND
$Comp
L CONN_01X03 J1
U 1 1 56FC0600
P 1200 1300
F 0 "J1" H 1200 1500 50  0000 C CNN
F 1 "CONN_01X03" V 1300 1300 50  0000 C CNN
F 2 "custom:TerminalBlock_Pheonix_MPT-5.08mm_3pol" H 1200 1300 60  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/on-shore-technology-inc/OSTTC032162/ED2610-ND/614559" H 1200 1300 60  0001 C CNN
	1    1200 1300
	-1   0    0    -1  
$EndComp
Text Label 1800 1200 0    60   ~ 0
+BATT
$Comp
L FUSE F1
U 1 1 56FC0AA0
P 4150 2450
F 0 "F1" H 4250 2500 50  0000 C CNN
F 1 "FUSE" H 4050 2400 50  0000 C CNN
F 2 "custom:blade_mount_custom" H 4150 2450 60  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/mpd-memory-protection-devices/BK-6010/BK-6010-ND/2330529" H 4150 2450 60  0001 C CNN
	1    4150 2450
	1    0    0    -1  
$EndComp
$Comp
L CONN_01X04 J2
U 1 1 56FC17BA
P 1200 1950
F 0 "J2" H 1200 2200 50  0000 C CNN
F 1 "CONN_01X04" V 1300 1950 50  0000 C CNN
F 2 "custom:TerminalBlock_Pheonix_MPT-5.08mm_4pol" H 1200 1950 60  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/on-shore-technology-inc/OSTTC042162/ED2611-ND/614560" H 1200 1950 60  0001 C CNN
	1    1200 1950
	-1   0    0    -1  
$EndComp
Text Label 4800 2450 0    60   ~ 0
GLVMS_A
Text Label 3500 2450 0    60   ~ 0
+BATT
Text Label 1800 1800 0    60   ~ 0
GLVMS_A
Text Label 4250 4550 0    60   ~ 0
MReset_A
Text Label 5950 4550 0    60   ~ 0
MReset_A
Text Label 4300 3350 0    60   ~ 0
SL
$Comp
L R R1
U 1 1 56FC3EA7
P 7850 3400
F 0 "R1" V 7930 3400 50  0000 C CNN
F 1 "10k" V 7850 3400 50  0000 C CNN
F 2 "Resistors_SMD:R_0805_HandSoldering" V 7780 3400 30  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/panasonic-electronic-components/ERJ-6ENF1002V/P10.0KCCT-ND/119248" H 7850 3400 30  0001 C CNN
	1    7850 3400
	0    -1   1    0   
$EndComp
Text Label 8400 3400 0    60   ~ 0
+5V
Text Label 8400 1600 0    60   ~ 0
Ctrl
Text Label 7000 3950 0    60   ~ 0
Ctrl
Text Label 7000 3800 0    60   ~ 0
+5V
Text Label 7350 3400 0    60   ~ 0
R1
Text Label 7000 4100 0    60   ~ 0
R1
Wire Wire Line
	7700 1400 7300 1400
Wire Wire Line
	7300 1600 7700 1600
Wire Wire Line
	5900 1550 5500 1550
Wire Wire Line
	5900 1450 5500 1450
Wire Wire Line
	4600 1700 5000 1700
Wire Wire Line
	4600 1300 5000 1300
Wire Wire Line
	3200 1550 2800 1550
Wire Wire Line
	3200 1450 2800 1450
Wire Wire Line
	4300 3350 4300 3750
Wire Wire Line
	4250 4550 4250 4150
Wire Wire Line
	4000 3550 4000 3750
Wire Wire Line
	3400 3550 3400 3850
Wire Wire Line
	3400 4350 3400 4150
Wire Wire Line
	4000 4350 4000 4150
Wire Wire Line
	1400 6100 1800 6100
Wire Wire Line
	1400 6000 1800 6000
Wire Wire Line
	8400 1400 8800 1400
Wire Wire Line
	9700 1300 9300 1300
Wire Wire Line
	3500 6000 3500 5800
Wire Wire Line
	3400 5700 3400 6000
Wire Wire Line
	6000 3350 6000 3750
Wire Wire Line
	5950 4550 5950 4150
Connection ~ 5400 3550
Wire Wire Line
	5400 3350 5400 3550
Connection ~ 5400 4350
Wire Wire Line
	5400 4550 5400 4350
Wire Wire Line
	5700 3550 5700 3750
Wire Wire Line
	5100 3550 5700 3550
Wire Wire Line
	5100 3550 5100 3850
Wire Wire Line
	5100 4350 5100 4150
Wire Wire Line
	5100 4350 5700 4350
Wire Wire Line
	5700 4350 5700 4150
Wire Wire Line
	8800 1300 8800 1300
Wire Wire Line
	9300 1600 9300 1600
Wire Wire Line
	9300 1700 9300 1700
Wire Wire Line
	9300 1400 9700 1400
Wire Wire Line
	9300 1500 9700 1500
Wire Wire Line
	8400 1500 8800 1500
Wire Wire Line
	8400 1700 8800 1700
Wire Wire Line
	1800 2800 1400 2800
Wire Wire Line
	1800 2700 1400 2700
Wire Wire Line
	1800 2600 1400 2600
Wire Wire Line
	1800 2500 1400 2500
Wire Wire Line
	1800 3200 1400 3200
Wire Wire Line
	1800 3300 1400 3300
Wire Wire Line
	1800 3400 1400 3400
Wire Wire Line
	1800 3500 1400 3500
Wire Wire Line
	1800 5000 1400 5000
Wire Wire Line
	1400 5100 1800 5100
Wire Wire Line
	1800 4900 1400 4900
Wire Wire Line
	1400 4800 1800 4800
Wire Wire Line
	1800 4400 1400 4400
Wire Wire Line
	1400 4300 1800 4300
Wire Wire Line
	1800 4200 1400 4200
Wire Wire Line
	1400 4100 1800 4100
Wire Wire Line
	1800 4000 1400 4000
Wire Wire Line
	1400 3900 1800 3900
Wire Wire Line
	1400 1900 1800 1900
Wire Wire Line
	1400 2100 1800 2100
Wire Wire Line
	1400 2000 1800 2000
Wire Wire Line
	1400 5600 1800 5600
Wire Wire Line
	1400 5500 1800 5500
Connection ~ 1600 1350
Wire Wire Line
	1600 1350 1800 1350
Wire Wire Line
	1600 1400 1400 1400
Wire Wire Line
	1600 1300 1600 1400
Wire Wire Line
	1400 1300 1600 1300
Wire Wire Line
	8400 4100 8800 4100
Wire Wire Line
	7400 3800 7000 3800
Wire Wire Line
	7000 3950 7400 3950
Wire Wire Line
	7000 4100 7400 4100
Wire Wire Line
	1400 1200 1800 1200
Wire Wire Line
	3500 2450 3900 2450
Wire Wire Line
	4400 2450 4800 2450
Wire Wire Line
	1400 1800 1800 1800
Wire Wire Line
	7700 3400 7350 3400
Wire Wire Line
	8400 1600 8800 1600
Wire Wire Line
	8000 3400 8400 3400
Wire Wire Line
	3400 3550 4000 3550
Wire Wire Line
	3700 3550 3700 3350
Connection ~ 3700 3550
Wire Wire Line
	3400 4350 4000 4350
Wire Wire Line
	3700 4350 3700 4550
Connection ~ 3700 4350
Wire Wire Line
	8800 3800 8400 3800
$Comp
L TPS27081A U3
U 1 1 56FC1E5B
P 7900 3950
F 0 "U3" H 7900 4250 60  0000 C CNN
F 1 "TPS27081A" H 7900 3650 60  0000 C CNN
F 2 "custom:SOT-23-6_custom" H 8100 4150 60  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/texas-instruments/TPS27081ADDCR/296-34970-1-ND/3671576" H 8100 4150 60  0001 C CNN
	1    7900 3950
	1    0    0    -1  
$EndComp
$Comp
L Footprint FTP1
U 1 1 56FC632B
P 2600 7400
F 0 "FTP1" H 2600 7300 60  0000 C CNN
F 1 "LAFAYETTE ECE" H 2600 7500 60  0000 C CNN
F 2 "custom:logo_lafayette_ece_32mmx11mm" H 2600 7400 60  0001 C CNN
F 3 "" H 2600 7400 60  0000 C CNN
	1    2600 7400
	1    0    0    -1  
$EndComp
$Comp
L Footprint FTP2
U 1 1 56FC6368
P 3200 7400
F 0 "FTP2" H 3200 7300 60  0000 C CNN
F 1 "Pawprint" H 3200 7500 60  0000 C CNN
F 2 "custom:lafLogo" H 3200 7400 60  0001 C CNN
F 3 "" H 3200 7400 60  0000 C CNN
	1    3200 7400
	1    0    0    -1  
$EndComp
$Comp
L Footprint FTP3
U 1 1 56FC63A7
P 3800 7400
F 0 "FTP3" H 3800 7300 60  0000 C CNN
F 1 "RoHS" H 3800 7500 60  0000 C CNN
F 2 "custom:ROHS" H 3800 7400 60  0001 C CNN
F 3 "" H 3800 7400 60  0000 C CNN
	1    3800 7400
	1    0    0    -1  
$EndComp
$Comp
L Footprint FTP4
U 1 1 56FC7301
P 4350 7400
F 0 "FTP4" H 4350 7300 60  0000 C CNN
F 1 "OSHW" H 4350 7500 60  0000 C CNN
F 2 "Symbols:Symbol_OSHW-Logo_SilkScreen" H 4350 7400 60  0001 C CNN
F 3 "" H 4350 7400 60  0000 C CNN
	1    4350 7400
	1    0    0    -1  
$EndComp
$Comp
L R R2
U 1 1 56FDE13C
P 9650 5000
F 0 "R2" V 9730 5000 50  0000 C CNN
F 1 "0" V 9650 5000 50  0000 C CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" V 9580 5000 30  0001 C CNN
F 3 "http://www.digikey.com/product-detail/en/panasonic-electronic-components/ERJ-3GEY0R00V/P0.0GCT-ND/134711" H 9650 5000 30  0001 C CNN
	1    9650 5000
	0    -1   1    0   
$EndComp
Wire Wire Line
	9100 5000 9500 5000
Wire Wire Line
	9800 5000 10200 5000
Wire Wire Line
	9100 5800 9500 5800
Wire Wire Line
	9800 5800 10200 5800
Wire Wire Line
	9100 6000 9500 6000
Wire Wire Line
	9800 6000 10200 6000
Wire Wire Line
	9100 6200 9500 6200
Wire Wire Line
	9800 6200 10200 6200
$Comp
L C C1
U 1 1 56FE0B1E
P 9650 5800
F 0 "C1" H 9675 5900 50  0000 L CNN
F 1 "C" H 9675 5700 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 9688 5650 30  0001 C CNN
F 3 "" H 9650 5800 60  0000 C CNN
	1    9650 5800
	0    1    1    0   
$EndComp
$Comp
L C C2
U 1 1 56FE0EB2
P 9650 6000
F 0 "C2" H 9675 6100 50  0000 L CNN
F 1 "C" H 9675 5900 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 9688 5850 30  0001 C CNN
F 3 "" H 9650 6000 60  0000 C CNN
	1    9650 6000
	0    1    1    0   
$EndComp
$Comp
L C C3
U 1 1 56FE0F1E
P 9650 6200
F 0 "C3" H 9675 6300 50  0000 L CNN
F 1 "C" H 9675 6100 50  0000 L CNN
F 2 "Resistors_SMD:R_0603_HandSoldering" H 9688 6050 30  0001 C CNN
F 3 "" H 9650 6200 60  0000 C CNN
	1    9650 6200
	0    1    1    0   
$EndComp
Text Label 9100 5000 0    60   ~ 0
R2
Text Label 10200 5000 0    60   ~ 0
GND
Text Label 10200 5800 0    60   ~ 0
Vout
Text Label 10200 6000 0    60   ~ 0
GND
Text Label 10200 6200 0    60   ~ 0
GND
Text Label 9100 5800 0    60   ~ 0
R1
Text Label 8800 4100 0    60   ~ 0
R2
Text Label 9100 6000 0    60   ~ 0
Vout
Text Label 9100 6200 0    60   ~ 0
+5V
Text Label 8800 3950 0    60   ~ 0
Vout
Wire Wire Line
	8800 3950 8400 3950
Text Notes 9000 5600 0    60   ~ 0
Do not populate C1, C2, and C3.
$EndSCHEMATC
